#ifndef ANALYZER_H
#define ANALYZER_H
#include "Station.h"

class Analyzer {
	public:
		void analyze(Station* station);
};
#endif