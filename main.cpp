#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

//header files
#include "Stat.h"
#include "Day.h"
#include "Station.h"
#include "Analyzer.h"
#include "Region.h"

bool split(const std::string& s, char delimiter, std::string elements[], int expectedNumberOfElements);

bool split(const std::string& s, char delimiter, std::string elements[], int expectedNumberOfElements) {
	std::stringstream ss;
	ss.str(s);
	std::string item;

	int i=0;
	while (std::getline(ss, item, delimiter) && i<expectedNumberOfElements) {
		elements[i++] = item;
	}
	return (i==expectedNumberOfElements);
}

int main(int argc, char* argv[]) {
	if (argc>1) {
		std::ifstream inputStream(argv[1], std::ios_base::in);
		Region region;
		region.load(inputStream);

		Analyzer analyzer;

		if (argc>2) {
			std::string stationId(argv[2]);
			Station* station = region.findStation(stationId);
			if (station!= nullptr)
				analyzer.analyze(station);
		}
		else {
			region.resetStationIteration();
			Station *station;
			while ((station = region.getNextStation()) != nullptr)
				analyzer.analyze(station);
		}
	} else {
		std::cout << "There were no arguments! Don't you like to argue?" << std::endl;
		std::cout << "Try including a csv file with the weather data you want analyzed as an argument." << std::endl;
	}

	return 0;
}








